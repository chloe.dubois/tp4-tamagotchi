// reload la page par click
const reloadBouton = document.getElementById("reload");
reloadBouton.addEventListener("click", function(event){
    location.reload()
});


// Définition des constantes pour les besoins
// faim
const faim = document.getElementById("faim");
const faimValeur = faim.querySelector(".valeur"); // chiffre de 0 à 100 %
const faimNiveau = faim.querySelector(".niveau"); // largeur de la barre

// soif 
const soif = document.getElementById("soif");
const soifValeur = soif.querySelector(".valeur"); // chiffre de 0 à 100 %
const soifNiveau = soif.querySelector(".niveau"); // largeur de la barre

// ennui
const ennui = document.getElementById("ennui");
const ennuiValeur = ennui.querySelector(".valeur"); // chiffre de 0 à 100 %
const ennuiNiveau = ennui.querySelector(".niveau"); // largeur de la barre

// fonction jauges : change le style des jauges (largeur, couleur) + innertext

function jauges(besoinScore, besoinNiveau, besoinValeur) {
        besoinNiveau.style.width = besoinScore + "%";
        besoinValeur.innerText = besoinScore;
        if (besoinScore < 15) {
            besoinNiveau.style.backgroundColor = "rgb(241, 147, 147)";
        } else if (besoinScore >= 15 && besoinScore < 30) {
            besoinNiveau.style.backgroundColor = "rgb(241, 197, 147)";
        } else if (besoinScore >= 30 && besoinScore < 50) {
            besoinNiveau.style.backgroundColor = "rgb(235, 241, 147)";
        } else if (besoinScore >= 50) {
            besoinNiveau.style.backgroundColor = "rgb(158, 241, 147)";
        }
};

// fonction gameover : affiche les lettres de game over (inclue dans fonction life)

const gameG = document.getElementById("game-g");
const gameA = document.getElementById("game-a");
const gameM = document.getElementById("game-m");
const gameE = document.getElementById("game-e");
const overO = document.getElementById("over-o");
const overV = document.getElementById("over-v");
const overE = document.getElementById("over-e");
const overR = document.getElementById("over-r");


function gameOver () {
    window.setTimeout (function(){
        gameG.style.display = "inline-block";
    }, 3000);
    window.setTimeout (function(){
        gameA.style.display = "inline-block";
    }, 3100);
    window.setTimeout (function(){
        gameM.style.display = "inline-block";
    }, 3200);
    window.setTimeout (function(){
        gameE.style.display = "inline-block";
    }, 3300);
    window.setTimeout (function(){
        overO.style.display = "inline-block";
    }, 3400);
    window.setTimeout (function(){
        overV.style.display = "inline-block";
    }, 3500);
    window.setTimeout (function(){
        overE.style.display = "inline-block";
    }, 3600);
    window.setTimeout (function(){
        overR.style.display = "inline-block";
    }, 3700);
}

// fonction vie : change la couleur et vitesse de battement du coeur, change l'image en fonction du niveau de vie

// constantes pour animal
const animal = document.getElementById("animal-image"); // images gif
const miam = document.getElementById("miam");

// constante pour coeur
const coeur = document.getElementById("heart"); //coeur svg
const coeurColor = document.getElementById("heart-path"); // couleur du coeur (path du svg)

function life() {
    const vieScore = (faimScore + soifScore + ennuiScore) / 3;
    if (vieScore == 0) {
        coeurColor.style.fill = ("rgb(129, 119, 107)");
        coeur.style.animationDuration = ("0s");

        window.setTimeout (function(){
            animal.src = "./assets/images/hot.gif";
            animal.alt = "hamster dying";  
            gameOver();
            window.clearInterval(lifeInterval);
        }, 500);
    } else {
        if (vieScore > 0 && vieScore < 15) {
            coeurColor.style.fill = ("rgb(241, 147, 147)");
            coeur.style.animationDuration = ("0.5s");
        } else if (vieScore >= 15 && vieScore < 40) {
            coeurColor.style.fill = ("rgb(235, 155, 168)");
            coeur.style.animationDuration = ("1s");
            animal.src = "./assets/images/angry.gif";  
            animal.alt = "angry hamster";  
        } else if (vieScore >= 40) {
            coeurColor.style.fill = ("pink");
            coeur.style.animationDuration = ("1.5s");
            animal.src = "./assets/images/happy.gif"; 
            animal.alt = "happy hamster";  
        }
    }
}



// Décrémentation du score par Interval + appel de la fonction jauges, appel de la fonction life

const start = document.getElementById("start"); // bouton pour démarrer le décompte
let commencer = false;

start.addEventListener("click", function(event){ 
    commencer = !commencer;
});


let faimScore = 100; // variables pour les scores
let soifScore = 100;
let ennuiScore = 100;


let faimInterval = window.setInterval(function(){
    if (commencer === true) {
        if (faimScore > 0) {
            faimScore -= 5;
            jauges(faimScore, faimNiveau, faimValeur);
        }
    }
}, 1100);

let soifInterval = window.setInterval(function(){
    if (commencer === true) {
        if (soifScore > 0) {
            soifScore -= 5;
            jauges(soifScore, soifNiveau, soifValeur);
        }
    }
}, 1300);

let ennuiInterval = window.setInterval(function(){
    if (commencer === true) {
        if (ennuiScore > 0) {
            ennuiScore -= 5;
            jauges(ennuiScore, ennuiNiveau, ennuiValeur);
        }
    }
}, 1600);

let lifeInterval = window.setInterval(life, 900);



// INCREMENTATION

// Choix du mode console vs boutons 

mode = document.getElementById("game-mode"); // bouton pour choisir le mode console vs boutons
modeConsole = document.getElementById("console-container"); // div contenant la console
modeBouton = document.getElementById("btn-container"); // div contenant les boutons

reglesBoutons = document.getElementById("regles-boutons"); // paragraphe de règles qui apparait selon le mode de jeu
reglesConsole = document.getElementById("regles-console"); // paragraphe de règles qui apparait selon le mode de jeu

mode.addEventListener("click", function(event){ 
    if (modeBouton.style.display !== 'none') {
        modeBouton.style.display = 'none';
    } else {
        modeBouton.style.display = 'block';
    }
    if (modeBouton.style.display == 'block') {
        modeConsole.style.display = 'none';
        reglesConsole.style.display = 'none';
        reglesBoutons.style.display = 'block';
    } else if (modeBouton.style.display == 'none') {
        modeConsole.style.display = 'block';
        reglesConsole.style.display = 'block';
        reglesBoutons.style.display = 'none';
    }
});

// Incrémentation du score avec le 'click' (mode boutons)
// boutons jeu
const faimBouton = document.getElementById("manger");
const soifBouton = document.getElementById("boire");
const ennuiBouton = document.getElementById("jouer");

faimBouton.addEventListener("click", function(event){
    if (commencer === true) {
        const vieScore = (faimScore + soifScore + ennuiScore) / 3;
        if (vieScore > 0 && faimScore < 100) {
            faimScore += 5;
            jauges(faimScore, faimNiveau, faimValeur);
            if (miam.innerText == ""){
                miam.innerText = "miam miam";
                setTimeout(function() {
                    miam.innerText = "";
                },800);    
            }
        }
    }
});

soifBouton.addEventListener("click", function(event){
    if (commencer === true) {
        const vieScore = (faimScore + soifScore + ennuiScore) / 3;
        if (vieScore > 0 && soifScore < 100) {
            soifScore += 5;
            jauges(soifScore, soifNiveau, soifValeur);
            if (miam.innerText == ""){
                miam.innerText = "glou glou";
                setTimeout(function() {
                    miam.innerText = "";
                },800);    
            }
        }
    }
});

ennuiBouton.addEventListener("click", function(event){
    if (commencer === true) {
        const vieScore = (faimScore + soifScore + ennuiScore) / 3;
        if (vieScore > 0 && ennuiScore < 100) {
            ennuiScore += 5;
            jauges(ennuiScore, ennuiNiveau, ennuiValeur);
            if (miam.innerText == ""){
                miam.innerText = "youpiii !";
                setTimeout(function() {
                    miam.innerText = "";
                },800);
            }
        }
    }
});

// Incrémentation du score avec l'input (mode console)

const commande = document.getElementById("console"); // input dans textarea
const feedback = document.getElementById("feedback"); // div de réponses


window.addEventListener("keydown", function(event){
    let feedbackText1 = document.createElement('p');
    let feedbackText2 = document.createElement('p');
    let feedbackText3 = document.createElement('p');
    const vieScore = (faimScore + soifScore + ennuiScore) / 3;
    
    if (commencer === true && vieScore > 0 && vieScore < 100) {
        commande.maxLength = 8; // bloque les nouvelles commandes si game over ou pause

        if (event.keyCode == 13) { // si la touche "enter" est pressée
            event.preventDefault();

            //commande noisette
            if (commande.value == "noisette") {
                feedbackText1.innerText = "> noisette";
                feedbackText2.innerText = "le hamster mange";
                feedbackText1.style.color = "white";
                feedbackText2.style.color = "rgb(158, 241, 147)";
                commande.value = "";
                if (vieScore > 0 && faimScore <= 70) {
                    faimScore += 30;
                    jauges(faimScore, faimNiveau, faimValeur);
                    if (miam.innerText == ""){
                        miam.innerText = "miam miam";
                        setTimeout(function() {
                            miam.innerText = "";
                        },800);    
                    }
                } else if (faimScore > 70) {
                    faimScore = 100;
                    jauges(faimScore, faimNiveau, faimValeur);
                    if (miam.innerText == ""){
                        miam.innerText = "miam miam";
                        setTimeout(function() {
                            miam.innerText = "";
                        },800);    
                    }
                }
            //commande eau
            } else if (commande.value == "eau") {
                feedbackText1.innerText = "> eau";
                feedbackText2.innerText = "le hamster boit";
                feedbackText1.style.color = "white";
                feedbackText2.style.color = "rgb(158, 241, 147)";
                commande.value = "";
                if (vieScore > 0 && soifScore <= 70) {
                    soifScore += 30;
                    jauges(soifScore, soifNiveau, soifValeur);
                    if (miam.innerText == ""){
                        miam.innerText = "glou glou";
                        setTimeout(function() {
                            miam.innerText = "";
                        },800);    
                    }
                } else if (soifScore > 70) {
                    soifScore = 100;
                    jauges(soifScore, soifNiveau, soifValeur);
                    if (miam.innerText == ""){
                        miam.innerText = "glou glou";
                        setTimeout(function() {
                            miam.innerText = "";
                        },800);    
                    }
                }            
            
            // commande roue
            } else if (commande.value == "roue") {
                feedbackText1.innerText = "> roue";
                feedbackText2.innerText = "le hamster court";
                feedbackText1.style.color = "white";
                feedbackText2.style.color = "rgb(158, 241, 147)";
                commande.value = "";
                if (vieScore > 0 && ennuiScore <= 70) {
                    ennuiScore += 30;
                    jauges(ennuiScore, ennuiNiveau, ennuiValeur);
                    if (miam.innerText == ""){
                        miam.innerText = "youpiii !";
                        setTimeout(function() {
                            miam.innerText = "";
                        },800);
                    }
                } else if (ennuiScore > 70) {
                    ennuiScore = 100;
                    jauges(ennuiScore, ennuiNiveau, ennuiValeur);
                    if (miam.innerText == ""){
                        miam.innerText = "youpiii !";
                        setTimeout(function() {
                            miam.innerText = "";
                        },800);
                    }                
                }

            // commande aide
            } else if (commande.value == "aide") {
                feedbackText1.innerText = "Tapez 'noisette' pour nourrir le hamster";
                feedbackText2.innerText = "Tapez 'eau' pour l'hydrater";
                feedbackText3.innerText = "Tapez 'roue' pour le faire courrir.";
                feedbackText1.style.color = "rgb(147, 219, 241)";
                feedbackText2.style.color = "rgb(147, 219, 241)";
                feedbackText3.style.color = "rgb(147, 219, 241)";
                commande.value = "";

            // commande invalide
            } else {
                feedbackText1.innerText = "commande invalide";
                feedbackText2.innerText = "Tapez 'aide' pour connaitre les commandes valides";
                feedbackText1.style.color = "rgb(241, 147, 147)";
                feedbackText2.style.color = "rgb(241, 147, 147)";
                commande.value = "";
            }

            setTimeout(function() {
                feedback.prepend(feedbackText1);
            },500);
            setTimeout(function() {
                feedback.prepend(feedbackText2);
            },800);
            setTimeout(function() {
                feedback.prepend(feedbackText3);
            },1100);
        }

    } else if (commencer === false || vieScore == 0 || vieScore == 100) {
        commande.maxLength = 0; // bloque les nouvelles commandes si game over ou pause
        feedbackText1.innerText = "";
        feedbackText2.innerText = "";
        feedbackText3.innerText = "";
    }
});



